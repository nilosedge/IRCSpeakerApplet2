package com.parelli.IRCSpeakerApplet;

public class Browser {
	private String userAgent;
	private String browserName = "unknown";
	private String OSName = "unknown";
	private String OSArch = "unknown";
	
	public Browser(String userAgent) {
		this.userAgent = userAgent;
		parseUserAgent(userAgent);
	}
	
	private void parseUserAgent(String ua) {
		int spaceSep = ua.indexOf(" ");
		int closeParen = ua.indexOf(")");
		
		//String s1 = ua.substring(0, spaceSep);
		String s2 = ua.substring(spaceSep + 2, closeParen);
		String s3 = ua.substring(closeParen + 1, ua.length());
		
		//see if we can get a browser match from s2
		String b = getBrowserName(s2);
		//if we got unknown see if we can get it from s3
		if(b.equalsIgnoreCase("unknown")) {
			b = getBrowserName(s3);
		}
		//thats all we can do so set it
		browserName = b;
		
		//try to get an OS match from s2
		OSName = getOSName(s2);
		
		//and the arch
		OSArch = getOSArch(s2);
		
	}
	
	private String getBrowserName(String part) {
		if(part.indexOf("AOL") >= 0) {
			return "AOL";
		}
		else if(part.indexOf("MSIE") >= 0) {
			return "MSIE";
		}
		else if(part.indexOf("Firefox") >= 0) {
			return "Firefox";
		}
		else if(part.indexOf("Safari") >= 0) {
			return "Safari";
		}
		else if(part.indexOf("Konqueror") >= 0) {
			return "Konqueror";
		}
		else if(part.indexOf("bot") >= 0) {
			return "bot";
		}
		else {
			return "unknown";
		}		
	}
	
	private String getOSName(String part) {
		//System.out.println("** checking for OS in: " + part);
		if(part.indexOf("compatible") >= 0) {
			return "Windows";
		}
		else if(part.indexOf("Windows") >= 0) {
			return "Windows";
		}
		else if(part.indexOf("Mac") >= 0 && part.indexOf("OS X") >= 0) {
			return "MacOSX";
		}
		else if(part.indexOf("Mac") >= 0 && part.indexOf("OS X") == -1) {
			return "Mac";
		}
		else if(part.indexOf("Linux") >= 0) {
			return "Linux";
		}
		else {
			return "unknown";
		}
	}
	
	private String getOSArch(String part) {
		if(part.indexOf("Linux") > 0) {
			if(part.indexOf("x86_64") >= 0) {
				return "x86_64";
			}
			else if(part.indexOf("i686") >= 0) {
				return "i686";
			}
			else {
				return "unknown";
			}
		}
		else if(part.indexOf("Mac") >= 0) {
			if(part.indexOf("PPC") >= 0) {
				return "PPC";
			}
			else {
				return "unknown";
			}
		}
		else {
			return "unknown";
		}
	}

	public String getBrowserName() {
		return browserName;
	}

	public String getOSArch() {
		return OSArch;
	}

	public String getOSName() {
		return OSName;
	}

	public String getUserAgent() {
		return userAgent;
	}
}

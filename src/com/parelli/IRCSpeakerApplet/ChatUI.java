package com.parelli.IRCSpeakerApplet;

import javax.swing.JPanel;

//import java.awt.AWTException;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridLayout;
//import java.awt.Robot;

import javax.swing.BorderFactory;
import javax.swing.DefaultListModel;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JColorChooser;
import javax.swing.JComboBox;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JList;
import javax.swing.JPopupMenu;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.SwingUtilities;
import javax.swing.UIDefaults;
import javax.swing.UIManager;

import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
//import java.awt.event.InputEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.StringTokenizer;

import javax.swing.JLabel;
import javax.swing.colorchooser.AbstractColorChooserPanel;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import org.schwering.irc.lib.IRCConnection;
import org.schwering.irc.lib.IRCUtil;

public class ChatUI extends JPanel implements ChangeListener {

	private static final long serialVersionUID = -7836541552235153032L;
	private JPanel topPanel = null;
	private JList nickList = null;	
	private JPanel bottomPanel = null;
	private JPanel inputPanel = null;
	private JPanel rightPanel = null;
	private JPanel smiliesPanel = null;
	private JTextField txtUserInput = null;
	private static DefaultListModel listModel;	
	private static ScrollableColoredChatPanel chatTextArea;
	private static JTextArea basicChatTextArea;
	//private static BasicChatPanel basicChatTextArea;
	private JColorChooser colorChooser;
	private JPanel topLeftPanel = null;
	private JPanel topRightPanel = null;
	private JLabel lblChattingAs = null;
	private JLabel lblNick = null;
	private JLabel lblINRoom = null;
	private JLabel lblChannel = null;
	private JLabel lblHelp = null;
	private JLabel lblMyColor = null;
	private JLabel lblFontSize = null;
	
	private JComboBox cmbFontChoose = null;
	private JCheckBox chkScroll = null;
	
	private JPopupMenu nickRightClickMenu = null;
	
	private IRCConnection conn;
	private String target = null;
	private String targetRoomName = null;
	private Nick myNick = null;
	private Browser browser = null;
	private static boolean useDecoratedGUI = true;
	private boolean isBrowserSupported = true;	
	
	private Color colorFlesh = Color.decode("#f4eddb");
	private Color colorBrownIntTitle = Color.decode("#3f320c");
	private Color colorDarkBrown = Color.decode("#67541d");
	private Color colorKhaki = Color.decode("#eddcb4");
	private Color colorBrownBorder = Color.decode("#e6c970");
	private Smilies sm = null;
	private Speaker speak = null;
	
	private String[] rooms = {"General Chat", "US Region Chat", "AU/NZ Region Chat", "UK Region Chat"};
	
	private int[] dupIcons = {1, 3, 13, 19, 21, 26};


	/**
	 * This is the default constructor
	 */
	public ChatUI(String target, Nick myNick, String browserString, Speaker speaker) {
		this.target = target;
		this.myNick = myNick;
		browser = new Browser(browserString);
		sm = new Smilies();
		speak = speaker;
		evaluateBrowser(browser);
		if(isBrowserSupported) {
			initialize();
		}
		else {
			this.add(new JLabel("Sorry, your browser is unsupported."));
		}
	}

	/**
	 * This method initializes this
	 * 
	 * @return void
	 */
	private void initialize() {
		redesignScroll();        
		
		colorChooser = new JColorChooser();
		colorChooser.getSelectionModel().addChangeListener(this);
		colorChooser.setPreviewPanel(new JPanel());
		// Override the chooser panels with our own
        AbstractColorChooserPanel panels[] = { new ColorChooserPanel2() };
        colorChooser.setChooserPanels(panels);	        
        
		this.setLayout(new BorderLayout());
		this.setSize(550, 350);		
		this.add(getJPanel(), BorderLayout.NORTH);			
		//this.add(getJList(), BorderLayout.EAST);
		this.add(getRightPanel(), BorderLayout.EAST);
		this.add(getJPanel2(), BorderLayout.SOUTH);
		
		if(useDecoratedGUI) {        
			chatTextArea = new ScrollableColoredChatPanel();
			this.add(chatTextArea, BorderLayout.CENTER);
		}
		else {
			basicChatTextArea = new JTextArea();
			this.add(basicChatTextArea, BorderLayout.CENTER);
		}			
		
		//set some labels
		setNick(myNick.getName());
		
		//changeRoom(targetRoomName);
		targetRoomName = "General Chat";
		lblChannel.setText(targetRoomName);
		
		//create the right-click popup menu
		nickRightClickMenu = getNickRightClickPopupMenu();
		
		txtUserInput.requestFocus();
	}

	/**
	 * This method initializes jPanel	
	 * 	
	 * @return javax.swing.JPanel	
	 */
	private JPanel getJPanel() {
		if (topPanel == null) {
			topPanel = new JPanel();
			topPanel.setBackground(colorFlesh);
			topPanel.setLayout(new BorderLayout());
			topPanel.add(getJPanel4(), BorderLayout.WEST);
			topPanel.add(getJPanel5(), BorderLayout.EAST);
		}
		return topPanel;
	}
	
	private JPopupMenu getNickRightClickPopupMenu() {
		if(nickRightClickMenu == null) {
			nickRightClickMenu = new JPopupMenu();
			// the top menu item
			JMenuItem mi = new JMenuItem("Send user a private message");
			mi.setActionCommand("Send user a private message");
			mi.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {					
					doNickRightClickAction(e.getActionCommand());
				}
			});
			nickRightClickMenu.add(mi);
			
			//the submenu for choosing colors
			JMenu colorMenu = new JMenu("Set user's chat color");
			JMenuItem colorItem = null;
			for(int i = 0; i < ChatColors.colors.length; i++) {
				colorItem = new JMenuItem(ChatColors.colors[i]);
				colorItem.setActionCommand(ChatColors.colors[i]);		
				colorItem.setIcon(new ColorIcon(Color.decode(ChatColors.colors[i]), 10, 10));
				colorItem.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {							
						doNickRightClickAction(e.getActionCommand());						
					}
				});
				colorMenu.add(colorItem);
			}
			nickRightClickMenu.add(colorMenu);	
		}
		return nickRightClickMenu;
	}

	/**
	 * This method initializes jList	
	 * 	
	 * @return javax.swing.JList	
	 */
	private JList getJList() {
		if (nickList == null) {
			listModel = new DefaultListModel();
			nickList = new JList(listModel);
			
			nickList.addMouseListener(mouseEventHandler);
			nickList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
			nickList.setFont(new Font("Verdana", Font.PLAIN, 12));
			nickList.setBorder(BorderFactory.createLineBorder(colorBrownBorder));
			nickList.setPreferredSize(new Dimension(150,100));
		}
		return nickList;
	}
	
	private JPanel getRightPanel() {
		if(rightPanel == null) {
			rightPanel = new JPanel();
			rightPanel.setLayout(new BorderLayout());
			rightPanel.add(getJList(), BorderLayout.CENTER);
			rightPanel.add(getSmiliesPanel(), BorderLayout.SOUTH);			
		}
		return rightPanel;
	}
	
	private JPanel getSmiliesPanel() {
		if(smiliesPanel == null) {
			smiliesPanel = new JPanel();
			smiliesPanel.setLayout(new GridLayout(6,6));			
			smiliesPanel.setBackground(Color.WHITE);
			// load the smilies into a button
			JButton bt = null;
			String[] smiliesText = sm.getSmiliesText();
			String[] smiliesFiles = sm.getSmiliesFiles();
			for(int i = 0; i < smiliesText.length; i++) {
				boolean isDupIcon = false;
				for(int j = 0; j < dupIcons.length; j++) {
					if(dupIcons[j] == i) {
						isDupIcon = true;
						//System.out.println("Disgarding dup icon " + dupIcons[j]);
					}
				}
				if(!isDupIcon) {
					bt = new JButton();
					bt.setPreferredSize(new Dimension(25,25));
					bt.setBackground(Color.WHITE);
					try {
						bt.setIcon(new ImageIcon(new URL("http://www.parellisavvyclub.com/forum/images/smilies/" + smiliesFiles[i])));
					} catch (MalformedURLException e) {
						System.out.println("Bad URL for smilie icon");
						e.printStackTrace();
					}		
					bt.setActionCommand(smiliesText[i]);
					bt.setToolTipText(String.valueOf(i));
					// add the action listener to the button
					bt.addActionListener(new ActionListener() {
						public void actionPerformed(ActionEvent e) {
							System.out.println("smilie click: " + e.getActionCommand());
							if(txtUserInput.getText().length() > 0) {
								txtUserInput.setText(txtUserInput.getText() + " " + e.getActionCommand());
							}
							else {
								txtUserInput.setText(e.getActionCommand());
							}
						}
					});
					smiliesPanel.add(bt);
				}				
			}	
		}
		return smiliesPanel;
	}

	/**
	 * This method initializes jPanel	
	 * 	
	 * @return javax.swing.JPanel	
	 */
	private JPanel getJPanel2() {
		if (bottomPanel == null) {
			lblMyColor = new JLabel();
			lblMyColor.setText("");
			lblMyColor.setToolTipText("my color");
			lblMyColor.setBackground(Color.black);
			lblMyColor.setForeground(Color.black);
			lblMyColor.setBorder(BorderFactory.createLineBorder(Color.black,2));
			lblMyColor.setOpaque(true);
			lblMyColor.setPreferredSize(new Dimension(25,25));
			FlowLayout flowLayout1 = new FlowLayout();
			flowLayout1.setAlignment(FlowLayout.LEFT);
			flowLayout1.setAlignment(FlowLayout.LEFT);
			bottomPanel = new JPanel();
			bottomPanel.setBackground(colorFlesh);
			bottomPanel.setLayout(flowLayout1);
			bottomPanel.add(getJPanel3(), null);
			if(useDecoratedGUI) {
				bottomPanel.add(lblMyColor, null);
				bottomPanel.add(colorChooser, null);
			}
			cmbFontChoose = new JComboBox();
			cmbFontChoose.setBackground(Color.WHITE);
			cmbFontChoose.addItem("8");
			cmbFontChoose.addItem("10");
			cmbFontChoose.addItem("12");
			cmbFontChoose.setSelectedItem("12");
			cmbFontChoose.addItem("14");
			cmbFontChoose.addItem("16");
			cmbFontChoose.addItem("20");
			cmbFontChoose.addItem("24");
			cmbFontChoose.addItem("30");
			cmbFontChoose.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					if(e.getActionCommand().equals("comboBoxChanged")) {
						System.out.println(cmbFontChoose.getSelectedItem());
						if(useDecoratedGUI) {
							chatTextArea.setFontSize(Integer.parseInt((String)cmbFontChoose.getSelectedItem()));
						} else {
							
							 System.out.println("Shouldnt see this.");
						}
					}
				}
			});
			
			lblFontSize = new JLabel("Font Size: ");
			bottomPanel.add(lblFontSize);
			bottomPanel.add(cmbFontChoose);
			
			chkScroll = new JCheckBox("Scroll chat text");
			chkScroll.setSelected(true);
			chkScroll.setBackground(colorFlesh);
			chkScroll.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					System.out.println("Scroll check action");
					if(chkScroll.isSelected()) {
						chatTextArea.setChatScrollEnabled(true);
					}
					else {
						chatTextArea.setChatScrollEnabled(false);
					}
				}
			});
			bottomPanel.add(chkScroll);
			
		}
		return bottomPanel;
	}

	/**
	 * This method initializes jPanel	
	 * 	
	 * @return javax.swing.JPanel	
	 */
	private JPanel getJPanel3() {
		if (inputPanel == null) {
			FlowLayout flowLayout = new FlowLayout();
			flowLayout.setAlignment(FlowLayout.LEFT);
			inputPanel = new JPanel();
			inputPanel.setBackground(colorFlesh);
			inputPanel.setLayout(flowLayout);
			inputPanel.add(getJTextField(), null);
		}
		return inputPanel;
	}

	/**
	 * This method initializes jTextField	
	 * 	
	 * @return javax.swing.JTextField	
	 */
	private JTextField getJTextField() {
		if (txtUserInput == null) {
			txtUserInput = new JTextField();			
			txtUserInput.setPreferredSize(new Dimension(400,25));
			txtUserInput.setFont(new Font("Verdana", Font.PLAIN, 12));
			txtUserInput.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					String t = txtUserInput.getText();
					if(t.length() > 0) {
						send(t);
						txtUserInput.setText("");
					}
				}
			});
		}
		return txtUserInput;
	}
	
	/**
	 * This method initializes jPanel	
	 * 	
	 * @return javax.swing.JPanel	
	 */
	private JPanel getJPanel4() {
		if (topLeftPanel == null) {
			FlowLayout flowLayout2 = new FlowLayout();
			flowLayout2.setAlignment(FlowLayout.LEFT);
			lblChannel = new JLabel();
			lblChannel.setToolTipText("change chat room");
			lblChannel.setText("Room");
			lblChannel.setPreferredSize(new Dimension(125,15));
			lblChannel.setFont(new Font("Verdana", Font.PLAIN, 12));
			lblChannel.setForeground(Color.BLUE);
			lblChannel.addMouseListener(new MouseListener() {
				public void mouseEntered(MouseEvent e) {
					lblChannel.setText("<html><u>" + targetRoomName + "</u></html>");
				}
				public void mouseClicked(MouseEvent e) {
					String newRoom = (String)JOptionPane.showInputDialog(null, "Choose a room to join", "Chage rooms", JOptionPane.PLAIN_MESSAGE, null, rooms, "help");
					if(newRoom != null) {
						System.out.println("selected new room: " + newRoom);
						changeRoom(newRoom);
					}
				}
				public void mousePressed(MouseEvent e) {
					// we use the click event
				}
				public void mouseReleased(MouseEvent e) {
					// we use the click event
				}
				public void mouseExited(MouseEvent e) {
					lblChannel.setText("<html>" + targetRoomName + "</html>");
				}
			});
			lblINRoom = new JLabel();
			lblINRoom.setText("Current room:");
			lblINRoom.setFont(new Font("Verdana", Font.PLAIN, 12));
			lblINRoom.setForeground(colorBrownIntTitle);
			lblNick = new JLabel();
			lblNick.setToolTipText("Your chat nickname");
			lblNick.setPreferredSize(new Dimension(150,15));
			lblNick.setFont(new Font("Verdana", Font.PLAIN, 12));
			lblNick.setText("nickname");
			lblNick.setForeground(colorBrownIntTitle);
			lblChattingAs = new JLabel();
			lblChattingAs.setText("Chatting as:");
			lblChattingAs.setFont(new Font("Verdana", Font.PLAIN, 12));
			lblChattingAs.setForeground(colorBrownIntTitle);
			topLeftPanel = new JPanel();
			topLeftPanel.setBackground(colorFlesh);
			topLeftPanel.setLayout(flowLayout2);
			topLeftPanel.setAlignmentX(0.5F);
			topLeftPanel.setAlignmentY(0.5F);
			topLeftPanel.add(lblChattingAs, null);
			topLeftPanel.add(lblNick, null);
			topLeftPanel.add(lblINRoom, null);
			topLeftPanel.add(lblChannel, null);
		}
		return topLeftPanel;
	}

	/**
	 * This method initializes jPanel	
	 * 	
	 * @return javax.swing.JPanel	
	 */
	private JPanel getJPanel5() {
		if (topRightPanel == null) {
			FlowLayout flowLayout3 = new FlowLayout();
			flowLayout3.setAlignment(FlowLayout.RIGHT);
			lblHelp = new JLabel();
			lblHelp.setToolTipText("Help");
			lblHelp.setText("Help");
			lblHelp.setPreferredSize(new Dimension(35,15));
			lblHelp.setFont(new Font("Verdana", Font.PLAIN, 12));
			lblHelp.setForeground(Color.BLUE);
			lblHelp.addMouseListener(new MouseListener() {
				public void mouseEntered(MouseEvent e) {
					lblHelp.setText("<html><u>Help</u></html>");
				}
				public void mouseClicked(MouseEvent e) {
					JOptionPane.showMessageDialog(null, getHelpMessage(), "Chat Help", JOptionPane.PLAIN_MESSAGE);
				}
				public void mousePressed(MouseEvent e) {
				}
				public void mouseReleased(MouseEvent e) {
				}
				public void mouseExited(MouseEvent e) {
					lblHelp.setText("Help");
				}
			});
			topRightPanel = new JPanel();
			topRightPanel.setBackground(colorFlesh);
			topRightPanel.setLayout(flowLayout3);
			topRightPanel.add(lblHelp, null);
		}
		return topRightPanel;
	}
	
	//*******************************************************************
	// changes the big color block when a user changes
    // their color
	public void stateChanged(ChangeEvent e) {
		Color nc = colorChooser.getColor();
		lblMyColor.setForeground(nc);
		lblMyColor.setBackground(nc);
		myNick.setColor(nc);
	}
	
	// makes the scrollbars pretty
	private void redesignScroll() {		
		UIDefaults defaults = UIManager.getDefaults();
		defaults.put("ScrollBar.thumbHighlight", colorDarkBrown.brighter());
		defaults.put("ScrollBar.thumbShadow", colorDarkBrown.darker());
		defaults.put("ScrollBar.shadow", colorKhaki.darker());
		defaults.put("ScrollBar.background", colorKhaki);
		defaults.put("ScrollBar.darkShadow", colorKhaki.darker().darker());
		defaults.put("ScrollBar.thumb", colorDarkBrown); //knob
		defaults.put("ScrollBar.highlight", Color.DARK_GRAY);	
	}
	
	private MouseListener mouseEventHandler = new MouseAdapter() {
        public void mousePressed(MouseEvent e) {
        	//System.out.println("mouse pressed");    
//            if (e.isPopupTrigger()) {      
//            	//System.out.println("  is a popup trigger");
//            	if(SwingUtilities.isRightMouseButton(e)) {
//            		//System.out.println("  is right click");     
// 					try	{
// 						Robot robot = new java.awt.Robot();
// 						robot.mousePress(InputEvent.BUTTON1_MASK);
// 						robot.mouseRelease(InputEvent.BUTTON1_MASK);
// 						//System.out.println("  robot has done its thing");     
// 					}
// 					catch(AWTException ae) { 
// 						System.out.println(ae); 
// 					}  
//            	}            	           	
//            }
//            else {
//            	//System.out.println("  press was not a popup trigger");
//            }
        }
        public void mouseReleased(MouseEvent e) {
        	//System.out.println("mouse released");                  	 
        	if(SwingUtilities.isRightMouseButton(e)) {
        		//System.out.println("  was right click");
				JList list = (JList)e.getSource();
				//System.out.println("  " + list.getSelectedValue() + " selected");				
				//System.out.println("  showing popup menu");
        		System.out.println("SVALUE: " + list.getSelectedIndex());
        		if(list.getSelectedIndex() == -1) {
        			JOptionPane.showConfirmDialog(null, "You must highlight a user with your left mouse button first!", "Oops!", JOptionPane.OK_CANCEL_OPTION, JOptionPane.INFORMATION_MESSAGE);
        		}
        		else {
        			nickRightClickMenu.show(nickList, e.getX(), e.getY());
        		}
	        	
        	}            	

        }
    };

    
	
	// Nickname stuff
	public void setNick(String nick) {
        lblNick.setText(nick);
    }
    public String getNick() {
        return lblNick.getText();
    }    
    
    // Channel stuff
    public String getChannelName() {
        return target;
    }
    
    // Nickname list stuff
    public void addNickToList(Object nick) {
    	listModel.addElement(nick);
    }
    public void removeNickFromList(String nickName) {
    	//listModel.removeElement(nick);
    	for(int i = 0; i < listModel.size(); i++) {
    		Nick n = (Nick)listModel.getElementAt(i);
    		if(nickName.equalsIgnoreCase(n.getName())) {
    			//System.out.println("found match");
    			listModel.remove(i);
    		}    		
    	}
    }
    public boolean isNickInList(String nickName) {
    	boolean isMatched = false;
    	for(int i = 0; i < listModel.size(); i++) {
    		Nick n = (Nick)listModel.getElementAt(i);
    		if(nickName.equalsIgnoreCase(n.getName())) {
    			//System.out.println("found match");
    			isMatched = true;
    		}    		
    	}
    	return isMatched;
    }
    public int getNickIndexInList(Object nick) {
    	return listModel.indexOf(nick);
    }
    public void updateNickInList(Object nick, int index) {
    	listModel.set(index, nick);
    }
    
    // Appends a line to the chat output screen
    public void appendOutputLine(String line, Color color) {
    	if(useDecoratedGUI) {
    		chatTextArea.appendLine(IRCUtil.parseColors(line), color);
    	}
    	else {
    		basicChatTextArea.append(line + "\n");
    	}
    }
    
    // sends a chat message to the server
    public void send(String msg) {
    	System.out.println("Sending msg: " + msg);
    	if(msg.length() == 0) {
    		return;
    	}
    	else {
    		StringTokenizer st = new StringTokenizer(msg);
    		
    		if(st.hasMoreTokens()) {
    		
    			String command = st.nextToken();

    			// this is a command
    			if(command.equalsIgnoreCase("/list")) {
    				System.out.println("Printing a list for: " + myNick.getName());
    				conn.doList(target);
    			}
    			else if(command.equalsIgnoreCase("/clear")) {
    				if(useDecoratedGUI) {
    					chatTextArea.clear();
    				}
    				else {
    					basicChatTextArea.setText("");
    				}
    			}
    			else if(command.equalsIgnoreCase("/msg") || command.equalsIgnoreCase("/message")) {
    				String person = st.nextToken();
    				if(person != null) {
    					String message = "";
    					String word = null;
    					while(st.hasMoreTokens()) {
    						word = st.nextToken();
    						message += " " + word;
    					}
    					conn.doPrivmsg(person, message);
    					appendOutputLine("< PrivMsg to " + person + "> " + sm.makeSmile(message), Color.decode("#c00000"));
    				}
    			}
    			else if(command.equalsIgnoreCase("/me")) {
    				String message = "";
					String word = null;
					while(st.hasMoreTokens()) {
						word = st.nextToken();
						message += " " + word;
					}
    				conn.doPrivmsg(target, myNick.getName() + message);
    				appendOutputLine("<" + myNick.getName() + "> " + myNick.getName() + " " + sm.makeSmile(message), myNick.getColor());
    			}
    			else {
    				//System.out.println("SENDING: " + msg);
    				//conn.send(msg.substring(1));
    				conn.doPrivmsg(target, msg);
    				appendOutputLine("<" + myNick.getName() + "> " + sm.makeSmile(msg), myNick.getColor());
    			}
    		}
    	}
    }
    
    // set the IRC connection for this object
    public void setIRCConnection(IRCConnection conn) {
    	System.out.println("setting connection object");
    	this.conn = conn;
    }
    
    private void evaluateBrowser(Browser b) {
    	String name = b.getBrowserName();
    	String os = b.getOSName();
    	String arch = b.getOSArch();
    	
    	// default is set to true, so just try to match browsers that
    	// have problems
    	
    	// 64bit linux is a no go
    	if(os.equalsIgnoreCase("Linux") && arch.equalsIgnoreCase("x86_64")) {
    		isBrowserSupported = false;
    	}
    	if(os.equalsIgnoreCase("MacOSX") && name.equalsIgnoreCase("Safari")) {
    		useDecoratedGUI = true;
    	}
    }
    
    //send newRoom without the #!
    private void changeRoom(String newRoom) {  
    	targetRoomName = newRoom;
    	conn.doPart(target);
    	//send("/clear");
		System.out.println("setting NAME: " + newRoom + ", CHANNEL: " + getChannelByName(newRoom));
        lblChannel.setText(newRoom);
        target = getChannelByName(newRoom);    
        //
        listModel.clear();
        //
		conn.doJoin(target);
    }
    
    private String getChannelByName(String name) {
    	if(name.equalsIgnoreCase("General Chat")) {
    		return "#main";
    	}
    	else if(name.equalsIgnoreCase("US Region Chat")) {
    		return "#US";
    	}
    	else if(name.equalsIgnoreCase("AU/NZ Region Chat")) {
    		return "#AU";
    	}
    	else if(name.equalsIgnoreCase("UK Region Chat")) {
    		return "#UK";
    	}
    	else {
    		System.out.println("uanble to determine room so sending to main");
    		return "#main";
    	}
    }

	public void doPong(String arg0) {
		System.out.println("PONG: " + arg0);
		conn.doPong(arg0);
	}
	
	public String makeSmile(String in) {
		return sm.makeSmile(in);
	}
	
	private void doNickRightClickAction(String actionCommand) {
		//System.out.println("Doing actionCommand: " + actionCommand);
		// either way we need to know which user is selected in the list
		Nick selectedNick = (Nick)nickList.getSelectedValue();		
		//System.out.println("Selected user: " + selectedNick.getName());
		
		if(actionCommand.equalsIgnoreCase("Send user a private message")) {
			if(selectedNick.getName().equalsIgnoreCase(myNick.getName())) {
				// don't send to yourself!
				JOptionPane.showConfirmDialog(null, "You can't send yourself a private message!", "Oops!", JOptionPane.OK_CANCEL_OPTION, JOptionPane.WARNING_MESSAGE);
			}
			else {
				// Show a pane to allow them to enter the pm
				String pMsg = JOptionPane.showInputDialog(null, "Private message to " + selectedNick.getName(), "Private Chat Message", JOptionPane.PLAIN_MESSAGE);
				if(pMsg.length() > 0) {
					send("/msg " + selectedNick.getName() + " " + pMsg);
				}				
			}
			
		}		
		else {
			if(selectedNick.getName().equalsIgnoreCase(myNick.getName())) {
				// can't set your own color choice here
				JOptionPane.showConfirmDialog(null, "To set your own chat color use the color picker at the bottom of the screen", "Oops!", JOptionPane.OK_CANCEL_OPTION, JOptionPane.WARNING_MESSAGE);
			}
			else {
				// has to be a color choice... loop and find it, then set it
				for(int i = 0; i < ChatColors.colors.length; i++) {
					if(ChatColors.colors[i].equalsIgnoreCase(actionCommand)) {
						System.out.println("Setting chat color for " + selectedNick.getName() + " to " + ChatColors.colors[i]);
						int nickIndex = getNickIndexInList(selectedNick);
						selectedNick.setColor(Color.decode(ChatColors.colors[i]));
						updateNickInList(selectedNick, nickIndex);
					}
				}
			}			
		}
	}
	
	 private String getHelpMessage() {
    	return 
    		"SEND A MESSAGE:\n" +
    		"  Type your message in the bottom text field and hit your <Enter> key to send the message\n\n" +
    		"SEND A PRIVATE MESSAGE (WHISPER): \n" +
    		"  In the list of users in the chat room that is on the right of the chat window, right-click\n" +
    		"  on the name of the user that you wish to send the private message to, and select the option\n" +
    		"  to send the user a private message.  Type your message in the window that pops up and click OK\n" +
    		"  The message will only be sent to the user you selected.\n\n" +
    		"CHANGE CHAT ROOMS: \n" +
    		"  Above the chat window is a blue link to the right of the 'Current Room' label that will tell you \n" +
    		"  the name of the room that you are currently in.  To change rooms, click on that link. This will open a window\n" +
    		"  with a pull-down menu of the available rooms.  Select the room you want and then click Ok. This\n" +
    		"  will cause you to leave the room you are in and enter the one you selected.\n\n" +
    		"USE A SMILEY:\n" +
    		"  Click the image of the smiley you want to have it inserted into the text you will send.  Once you have \n" +
    		"  selected a smiley you will still have to click into the bottom text field (where the smiley code will be) \n" +
    		"  and then hit your <Enter> key to send the message.\n\n"+
    		"CHANGE THE COLOR YOUR CHAT APPEARS AS: \n" +
    		"  Select the color you want your chat messages to appear as by clicking on one of the color blocks in the \n" +
    		"  bottom color picker next to the chat text field.  NOTE that this color selection will ONLY be the way that you\n" +
    		"  see yourself chatting... others can choose how they want your chat to be displayed. \n\n" +
    		"CHANGE THE CHAT COLOR OF ANOTHER USER:\n" +
    		"  In the list of users in the chat room that is on the right of the chat window, right-click\n" +
    		"  on the name of the user that you wish to change the chat color of and select the option to change\n" +
    		"  that user's chat color, and then select the color you want them to appear as.  NOTE that these\n" +
    		"  color selections will ONLY be for how you view the chat... others will not see or have access\n" +
    		"  to the colors that you have selected.\n\n" +
    		"CLEAR THE CHAT SCREEN: \n" +
    		"  Type '/clear' in the bottom text field and hit your <Enter> key\n\n" +
    		"CHANGE THE CHAT TEXT FONT SIZE: \n " +
    		"  Use the pull-down menu to the right of the color picker at the bottom of the page to change the size\n" +
    		"  that chat messages appear in the main chat window.\n\n";
    		
    }

	public Speaker getSpeaker() {
		return speak;
	}
    
}  //  @jve:decl-index=0:visual-constraint="10,10"
 
package com.parelli.IRCSpeakerApplet;

import java.awt.Color;
import java.util.ArrayList;
import java.util.Random;
import java.util.StringTokenizer;

import org.schwering.irc.lib.IRCEventAdapter;
import org.schwering.irc.lib.IRCEventListener;
import org.schwering.irc.lib.IRCModeParser;
import org.schwering.irc.lib.IRCUser;

public class ChatListener extends IRCEventAdapter implements IRCEventListener {
	
	private ChatUI ci = null;
	
	private String lastReply1 = null;
	private String lastReply2 = null;
	private ArrayList nickList;
	private boolean speakchat = true;
	private Speaker speak = null;
	private Random rnd = new Random(System.currentTimeMillis());
	
	public ChatListener(ChatUI chat) {
		ci = chat;
		speak = chat.getSpeaker();
	}
	
	public void speakingText(boolean speaking) {
		speakchat = speaking;
	}

	public void onRegistered() {
		ci.appendOutputLine("Registration successful", Color.BLACK);		
	}

	public void onDisconnected() {
		ci.appendOutputLine("Disconnected", Color.BLACK);
	}

	public void onError(String arg0) {
		ci.appendOutputLine("ERROR: " + arg0, Color.BLACK);
	}

	public void onError(int arg0, String arg1) {
		ci.appendOutputLine("ERROR: " + arg0 + " : " + arg1, Color.BLACK);
	}

	public void onInvite(String arg0, IRCUser arg1, String arg2) {
		// TODO Auto-generated method stub
	}

	public void onJoin(String arg0, IRCUser arg1) {
		System.out.println("got onJoin event");
		speak.sayText("Welcome, " + arg1.getNick() + " has entered the room.");
		ci.appendOutputLine("Welcome! " + arg1.getNick() + " has entered the room", Color.BLACK);		//ChatUI.addNickToList(arg1.getNick());
		
		if(!ci.isNickInList(arg1.getNick())) {
			if(nickList == null) {
				nickList = new ArrayList();
			}
			Nick newNick = new Nick(arg1.getNick(), getRandomColor());
			nickList.add(newNick);
			ci.addNickToList(newNick);
		}
	}

	public void onKick(String arg0, IRCUser arg1, String arg2, String arg3) {
		ci.removeNickFromList(arg1.getNick());
	}

	public void onMode(String arg0, IRCUser arg1, IRCModeParser arg2) {
		// TODO Auto-generated method stub
	}

	public void onMode(IRCUser arg0, String arg1, String arg2) {
		// TODO Auto-generated method stub
	}

	public void onNick(IRCUser arg0, String arg1) {
		// TODO Auto-generated method stub
	}

	public void onNotice(String arg0, IRCUser arg1, String arg2) {
		// TODO Auto-generated method stub
	}

	public void onPart(String arg0, IRCUser arg1, String arg2) {
		speak.sayText(arg1.getNick() + " has left the room, Goodbye.");
		ci.appendOutputLine(arg1.getNick() + " has left the room", Color.BLACK);
		ci.removeNickFromList(arg1.getNick());
	}

	public void onPing(String arg0) {
		System.out.println("PING: " + arg0);
		ci.doPong(arg0);
	}

	public void onPrivmsg(String arg0, IRCUser arg1, String arg2) {
		Nick n = null;
		Color c = null;
		String myNick = ci.getNick();
		
		System.out.println("Arg0: " + arg0 + " Arg1: " + arg1 + " Arg2: " + arg2);
		
		//System.out.println("Got msg from IRCUser " + arg1.getNick());
		for(int i =0; i < nickList.size(); i++) {
			n = (Nick)nickList.get(i);
			//System.out.println("Is " + arg1.getNick() + " = " + n.getName());
			if(n.getName().equals(arg1.getNick())) {
				c = n.getColor();
				if(c == null) {
					c = Color.PINK;
				}
				//System.out.println("yes");
				//break;
			}
			else {
				//System.out.println("no");
			}
		}

		if(arg0.equals(myNick)) {
			speak.sayText("Private Message from, " + arg1.getNick() + ". " + arg1.getNick() + " says, " + arg2);
			ci.appendOutputLine("< PrivMsg from " + arg1.getNick() + "> " + ci.makeSmile(arg2), Color.decode("#800080"));
		} else {
			speak.sayText(arg1.getNick() + " says, " + arg2);
			ci.appendOutputLine("<" + arg1.getNick() + "> " + ci.makeSmile(arg2), c);
		}
	}

	public void onQuit(IRCUser arg0, String arg1) {
		ci.appendOutputLine(arg0.getNick() + " has logged out", Color.BLACK);
		ci.removeNickFromList(arg0.getNick());
	}

	public void onReply(int arg0, String arg1, String arg2) {
		//this is a bit of a hack...
		
		//we want to get tht output of some commands but not all.  when a command
		//is sent to the server it does a reply (first line) and then does another reply
		//of what command the first reply was the output of.  so we need to save the last 
		//2 lines we get at any given time, and then we can decide if we need to 
		//do anything with them
		
		lastReply1 = lastReply2;
		lastReply2 = arg2;
		processReply();

	}

	public void onTopic(String arg0, IRCUser arg1, String arg2) {
		// TODO Auto-generated method stub
	}

	public void unknown(String arg0, String arg1, String arg2, String arg3) {
		// TODO Auto-generated method stub
	}
	
	private void processReply() {
		//if lastReply2 contains "/NAMES" then we need to take the names list from 
		//lastReply1 and add them to the list (if they are not there already)
		if(lastReply2.indexOf("/NAMES") >= 0) {
			System.out.println("refreshing names list");
			StringTokenizer t = new StringTokenizer(lastReply1, " ");
			String token = null;
			while(t.hasMoreTokens()) {
				token = t.nextToken();			
				// remove the @ stuff from nicknames
				String noPrefixNick = token;
				if(noPrefixNick.startsWith("@")) {
					noPrefixNick = noPrefixNick.substring(1);
				}
				
				if(!ci.isNickInList(noPrefixNick)) {
					if(nickList == null) {
						nickList = new ArrayList();
					}
					Nick newNick = new Nick(noPrefixNick, getRandomColor());
					nickList.add(newNick);
					ci.addNickToList(newNick);
				}
			}
		}
	}
	
	private Color getRandomColor() {		
		int c = 1 + rnd.nextInt(ChatColors.colors.length - 1);
		System.out.println("fetched " + ChatColors.colors[c] + " as random color at " + System.currentTimeMillis());	
		return Color.decode(ChatColors.colors[c]);
	}

}
